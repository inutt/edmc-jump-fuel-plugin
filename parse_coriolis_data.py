#!/usr/bin/env python3
import sys
import subprocess
import json
import os
import json
from tempfile import TemporaryDirectory


OUTPUT_FILENAME = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), "coriolis_data.json")


SHIP_FILENAME_TO_CORIOLIS_NAME_MAP = {
    'diamondback_scout': 'diamondback',
}
SHIP_FILENAME_TO_FDEV_NAME_MAP = {
    'adder':                'adder',
    'anaconda':             'anaconda',
    'asp':                  'asp',
    'asp_scout':            'asp_scout',
    'beluga':               'belugaliner',
    'cobra_mk_iii':         'cobramkiii',
    'cobra_mk_iv':          'cobramkiv',
    'imperial_cutter':      'cutter',
    'diamondback_scout':    'diamondback',
    'diamondback_explorer': 'diamondbackxl',
    'dolphin':              'dolphin',
    'eagle':                'eagle',
    'imperial_courier':     'empire_courier',
    'imperial_eagle':       'empire_eagle',
    'imperial_clipper':     'empire_trader',
    'federal_corvette':     'federation_corvette',
    'federal_dropship':     'federation_dropship',
    'federal_assault_ship': 'federation_dropship_mkii',
    'federal_gunship':      'federation_gunship',
    'fer-de-lance':         'ferdelance',
    'hauler':               'hauler',
    'keelback':             'independant_trader',
    'krait_mkii':           'krait_mkii',
    'krait_phantom':        'krait_light',
    'mamba':                'mamba',
    'orca':                 'orca',
    'python':               'python',
    'sidewinder':           'sidewinder',
    'type_6_transporter':   'type6',
    'type_7_transport':     'type7',
    'type_9_heavy':         'type9',
    'type_10_defender':     'type9_military',
    'alliance_chieftain':   'typex',
    'alliance_crusader':    'typex_2',
    'alliance_challenger':  'typex_3',
    'viper':                'viper',
    'viper_mk_iv':          'viper_mkiv',
    'vulture':              'vulture',
}
def ship_filename_to_coriolis_name(filename: str):
    filename = filename[:-5]   # Remove the trailing ".json"
    if filename in SHIP_FILENAME_TO_CORIOLIS_NAME_MAP:
        return SHIP_FILENAME_TO_CORIOLIS_NAME_MAP[filename]
    return filename
def ship_filename_to_fdev_name(filename: str):
    filename = filename[:-5]   # Remove the trailing ".json"
    if filename in SHIP_FILENAME_TO_FDEV_NAME_MAP:
        return SHIP_FILENAME_TO_FDEV_NAME_MAP[filename]
    return filename

#ORDERED_ARMOUR_TYPE_LIST = ["lightweight", "reinforced", "military", "mirrored", "reactive"]
ORDERED_ARMOUR_TYPE_LIST = ["grade1", "grade2", "grade3", "mirrored", "reactive"]


if __name__ == "__main__":
    with TemporaryDirectory() as tmp_dir:
        try:
            subprocess.run(["git", "clone", "https://github.com/EDCD/coriolis-data.git"], check=True, cwd=tmp_dir)
            pass
        except subprocess.CalledProcessError as e:
            sys.exit(1)

        output = {
            "ships": {},
            "modules": {},
        }

        ship_dir = os.path.join(tmp_dir, "coriolis-data", "ships")
        for filename in os.listdir(ship_dir):
            if not filename.endswith(".json"):
                continue

            with open(os.path.join(ship_dir, filename)) as file:
                data = json.load(file)
                data = data[ship_filename_to_coriolis_name(filename)]

                output["ships"][ship_filename_to_fdev_name(filename)] = {
                    "name": data["properties"]["name"],
                    "hull_mass": data["properties"]["hullMass"],
                    "reserve_tank_capacity": data["properties"]["reserveFuelCapacity"],
                }

                for (index, bulkhead) in enumerate(data["bulkheads"]):
                    armour_type = f"{ship_filename_to_fdev_name(filename)}_armour_{ORDERED_ARMOUR_TYPE_LIST[index]}"
                    output["modules"][armour_type] = { "mass": bulkhead["mass"] }


        module_dir = os.path.join(tmp_dir, "coriolis-data", "modules", "hardpoints")
        for filename in os.listdir(module_dir):
            if not filename.endswith(".json"):
                continue

            with open(os.path.join(module_dir, filename)) as file:
                data = json.load(file)
                for key in data:
                    modules = data[key]
                    for module in modules:
                        module_id = module["symbol"].lower()
                        if "mass" in module:
                            output["modules"][module_id] = { "mass": module["mass"] }
                        else:
                            output["modules"][module_id] = { "mass": 0 }

        module_dir = os.path.join(tmp_dir, "coriolis-data", "modules", "internal")
        for filename in os.listdir(module_dir):
            if not filename.endswith(".json"):
                continue

            with open(os.path.join(module_dir, filename)) as file:
                data = json.load(file)
                for key in data:
                    modules = data[key]
                    for module in modules:
                        module_id = module["symbol"].lower()
                        if "mass" in module:
                            output["modules"][module_id] = { "mass": module["mass"] }
                        else:
                            output["modules"][module_id] = { "mass": 0 }
                        
                        # Pull out the guardian FSD boost too for those modules
                        if module["grp"] == "gfsb":
                            output["modules"][module_id]["boost"] = module["jumpboost"]

        module_dir = os.path.join(tmp_dir, "coriolis-data", "modules", "standard")
        for filename in os.listdir(module_dir):
            if not filename.endswith(".json"):
                continue

            with open(os.path.join(module_dir, filename)) as file:
                data = json.load(file)
                for key in data:
                    modules = data[key]
                    for module in modules:
                        module_id = module["symbol"].lower()
                        if "mass" in module:
                            output["modules"][module_id] = { "mass": module["mass"] }
                        else:
                            output["modules"][module_id] = { "mass": 0 }

                        # FSDs and fuel tanks need additional data storing
                        if module["grp"] == "fsd":
                            output["modules"][module_id]["maximum_fuel"] = module["maxfuel"]
                            output["modules"][module_id]["optimal_mass"] = module["optmass"]
                            output["modules"][module_id]["linear_constant"] = module["fuelmul"]
                            output["modules"][module_id]["power_constant"] = module["fuelpower"]

                        if module["grp"] == "ft":
                            output["modules"][module_id]["capacity"] = module["fuel"]

    with open(OUTPUT_FILENAME, "w") as output_file:
        json.dump(output, output_file, indent="\t")
        print(f"\nOutput written to {OUTPUT_FILENAME}")
