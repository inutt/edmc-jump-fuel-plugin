# EDMC Jump Fuel Plugin

A plugin for the [Elite Dangerous](https://www.elitedangerous.com/) companion
utility [EDMC](https://github.com/EDCD/EDMarketConnector/) plugin to help
visualise where you'll be able to refuel along your plotted navigation route.


## Screenshots

### Route mode

This mode is used when a navigational route is plotted from the galaxy maps or
by other means. Fuel usage is recalculated when any factor affecting it changes
so the display will update if, for example, you collect some cargo that causes
your ship's mass to increase.

![Screenshot](images/route_mode_annotated.png)

1.  Jumps on route (remaining / total).
2.  Remaining total jump distance. Note that this will usually be slightly
    longer than the straight line distance to the destination shown on the
    galaxy map.
3.  Current system's star class indicator.
4.  Star systems along the route that can be reached with current fuel supplies
    in route order, left to right.
5.  Star systems along the route that cannot be reached without refuelling.

See [below](#star-class-indicators) for an explanation of the colours used.


### Fuel mode

This mode is used when no navigation route is plotted.

![Screenshot](images/fuel_mode_annotated.png)

1.  Placeholder for route summary (unused in fuel mode).
2.  Current system's star class indicator.
3.  Bar representing the level that the ship's main fuel tank is filled to. This
    will become red when the level is low.
4.  Exact fuel amount in main tank and numeric proportion filled.


## Installation/Updating

Download the latest release zip from the releases tab, and unpack it into your
EDMC plugins directory. (The plugins tab in EDMC's settings has a button to open
the appropriate directory, or see [EDMC's plugin
documentation](https://github.com/EDCD/EDMarketConnector/wiki/Plugins#more-info)
for how to locate it manually).

Your plugins directory should now have a directory called
`edmc-jump-fuel-plugin`, which should contain some or all of the files from this
repository. The files `load.py` and `coriolis.json` are required as they
contains the plugin code and required data, other files are optional and should
not affect the plugin's functionality.


## Star class indicators

-   ![Scoopable star](images/star_class_scoopable.png) Main sequence star that
    fuel can be scooped from, assuming you have a suitable fuel scoop equipped.
-   ![Non-scoopable star](images/star_class_non_scoopable.png) Star from which
    fuel cannot be scooped.
-   ![White dwarf star](images/star_class_white_dwarf.png) White dwarf star.
    Cannot be scooped in the normal way, but flying through the polar radiation
    cone with a fuel scoop equipped will supercharge your FSD to 150%
    of its normal range or efficiency.
-   ![Neutron star](images/star_class_neutron.png) Neutron star.
    Cannot be scooped in the normal way, but flying through the polar radiation
    cone with a fuel scoop equipped will supercharge your FSD to 400%
    of its normal range or efficiency.
-   ![Unreachable route star](images/star_class_unreachable.png) Star on your
    plotted navigation route that cannot be reached with your current fuel reserves.
-   ![Unknown star](images/star_class_unknown.png) Star class could not be
    determined.

## Known issues

-   Colours and symbols are only tested with EDMC's dark theme, and may not be
    properly visible on other themes.
-   Jumponium (synthesised) FSD boosts are not currently taken into account.
-   Jet cone boosts from white dwarf and neutron stars *may* not be correctly
    taken into account.
-   The estimated fuel usage for jumps may differ from in-game calculations by a
    small amount. Errors of up to 10% have been observed, but in almost all
    cases the error appears to be below 3%.
-   When in an SRV, Apex shuttle, or on foot, the fuel level indicator will be
    incorrect.
-   When first starting the game or after restarting EDMC, the current system's
    star class will show as unknown until at least one jump is performed.
-   The route indicators may be shown incorrectly during a hyperspace jump due
    to game events being processed in the wrong order. This issue should correct
    itself on arrival in the target system.


## Acknowledgments

-   [Elite Dangerous Community
    Developers](https://github.com/EDCD/coriolis-data) for the collected ship
    and module data
-   [furrycat on the Frontier
    forums](https://forums.frontier.co.uk/threads/the-science-of-the-guardian-fsd-booster-2-0.436365/)
    for a detailed analysis of how guardian FSD boosters affect fuel use
-   Repository icon is [No
    Fuel](https://thenounproject.com/icon/no-fuel-4394099/) by shashank singh
    from [NounProject.com](https://thenounproject.com/)
