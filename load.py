import tkinter as tk
import logging
import math
import json
import functools
import os
from typing import Optional, Dict, Any

import l10n
import requests

import edmc_data
from config import appname, user_agent
from theme import theme

# Constants that should probably be configurable
MAX_JUMPS_TO_SHOW = 10

COLOUR_NORMAL = "#f07b05"
COLOUR_DIMMED = "#702d00"
COLOUR_GREYED = "#383838"
COLOUR_RED    = "#c90a20"
COLOUR_BLUE   = "#00ccff"


_ = functools.partial(l10n.Translations.translate, context=__file__)

class PersistentData:
    def __init__(self):
        self.session: requests.Session = requests.Session()
        self.session.headers['User-Agent'] = user_agent
        self.plugin_name = os.path.basename(os.path.dirname(__file__))

        self.location: Optional[str] = None
        self.remaining_jumps: int = 0
        self.main_tank: float = 0
        self.reservoir: float = 0
        self.fuel_capacity: float = 0
        self.nav_route: Optional[list] = None
        self.route_index: int = 0
        self.cargo_mass: int = 0
        self.ship_id: Optional[str] = None
        self.module_mass: float = 0
        self.fsd_data: Optional[dict] = None
        self.max_jump_range: float = 0
        self.show_fuel: bool = False
        self.show_jumps: bool = False
        self.last_jump_dist: float = 0.0
        self.last_fuel_used: float = 0.0
        self.pause_fuel_calculations: bool = False
        self.last_fuel_estimate: float = 0.0
        self.system_pos: tuple(float, float, float) = (None, None, None)
        self.loadout_max_jump_range: float = 0.0
        self.current_star_class: str = ""

this = PersistentData()
logger = logging.getLogger(f'{appname}.{this.plugin_name}')

# Load in the ship and module data (mostly mass values)
with open(os.path.join(os.path.dirname(__file__), 'coriolis_data.json'), 'r', encoding='utf-8') as file:
    CORIOLIS_DATA = json.load(file)


def get_distance_between(start, end):
    x = end[0] - start[0]
    y = end[1] - start[1]
    z = end[2] - start[2]
    return math.sqrt(x*x + y*y + z*z)


def fuel_for_jump(distance: float, mass_adjustment: float = 0.0):
    linear_constant = this.fsd_data["linear_constant"]
    power_constant = this.fsd_data["power_constant"]
    fsd_optimal_mass = this.fsd_data["optimal_mass"]
    hull_mass = CORIOLIS_DATA["ships"][this.ship_id]["hull_mass"]
    if not hull_mass:
        logger.debug("Not calculating jump fuel as ship's hull mass is unknown")
        return None

    total_mass = hull_mass + this.module_mass + this.cargo_mass + this.main_tank + this.reservoir
    total_mass -= mass_adjustment
    range_boost = 0
    if this.guardian_booster_data is not None:
        range_boost = this.guardian_booster_data["boost"]
    maximum_range = max_jump_range()

    fuel = (distance * total_mass * maximum_range) / (fsd_optimal_mass * (maximum_range + range_boost))
    fuel **= power_constant
    fuel *= linear_constant

    return fuel


def max_jump_range():
    '''Get the maximum jump range, without taking any guardian FSD boosts into account'''
    linear_constant = this.fsd_data["linear_constant"]
    power_constant = this.fsd_data["power_constant"]
    fsd_optimal_mass = this.fsd_data["optimal_mass"]
    ship_mass = CORIOLIS_DATA["ships"][this.ship_id]["hull_mass"]
    max_fuel = this.fsd_data["maximum_fuel"]

    # Only do fuel calculations if we know the ship's mass
    if ship_mass is not None:
        total_mass = ship_mass + this.module_mass   # Cargo and fuel mass aren't counted for maximum range
        total_mass += this.fsd_data['maximum_fuel'] # ...except for the fuel required for the max range jump
        this.max_jump_range = ((max_fuel / linear_constant) ** (1.0 / power_constant)) * fsd_optimal_mass / total_mass
        return this.max_jump_range
    else:
        this.max_jump_range = None
        return None


def plugin_start3(plugin_dir: str) -> str:
    return this.plugin_name

def plugin_stop() -> None:
    this.session.close()

def prefs_changed(cmdr: str, is_beta: bool) -> None:
    update_status()


def plugin_app(parent):
    frame = tk.Frame(parent)

    this.route_progress_label = tk.Label(
        master = frame,
        text = f'{_("Remaining jumps")}:',
    )
    this.route_progress_label.grid(row=0, column=0, sticky=tk.E)

    this.route_progress_value = tk.Label(
        master = frame,
        text = f'{_("---")} / {_("---")}',
    )
    this.route_progress_value.grid(row=0, column=1, sticky=tk.W, columnspan=2)

    this.fuel_jumps_label = tk.Label(
        master = frame,
        text = f'{_("Jumps in fuel range")}:',
    )
    this.fuel_jumps_label.grid(row=1, column=0, sticky=tk.E)

    this.fuel_jumps_value_container = tk.Frame(
        master = frame,
    )
    this.fuel_jumps_value_container.grid(row=1, column=1, pady=(3,0), sticky=tk.W+tk.E) # y-padding here lines the text and non-text portions up (approximately)

    this.fuel_level_label = tk.Label(
        master = frame,
        text = '',
        justify = tk.LEFT,
    )
    this.fuel_level_label.grid(row=1, column=2, sticky=tk.W)

    frame.columnconfigure(1, weight=1)

    this.frame = frame
    return frame


def star_class_is_scoopable(star_class: str):
    return star_class[0] in 'KGBFOAM'


def star_class_to_fuel_colour(star_class: str):
    if star_class is None or star_class == "":
        return '#222222', '#222222'

    primary = COLOUR_NORMAL if star_class[0] in 'KGBFOAM' else COLOUR_DIMMED
    secondary = primary
    if star_class[0] == "D":
        # White dwarf
        primary = COLOUR_BLUE
        secondary = "#ffffff"
    if star_class[0] == "N":
        # Neutron star
        primary = COLOUR_BLUE
        secondary = COLOUR_BLUE
    return (primary, secondary)


def get_route_index_of(star: str):
    for i, route_star in enumerate(this.nav_route):
        if route_star['StarSystem'] == star:
            return i
    return None


def process_nav_route():
    # Calculate how far along the route we can get with the current fuel supply
    # TODO: Handle JetConeBoost event
    if this.nav_route is None or len(this.nav_route) == 0 or this.fsd_data is None:
        # Can't calculate fuel required for jumps
        return

    this.max_jump_range = max_jump_range()
    cumulative_fuel_used = 0
    this.last_route_index_we_have_fuel_for = len(this.nav_route) - 1
    reached_current_location_in_route = False
    found_last_fuel_index = False

    logger.debug("-------- Begin jump fuel estimates --------")
    for i, route_star in enumerate(this.nav_route):
        if i == len(this.nav_route)-1:
            # Skip the last star in the route, as we don't know where we're going after that and can't calculate the jump data
            continue

        if not this.current_star_class and i == 0:
            # First star in the route is the current location if we're plotting a new route, so we can update the current location if not already known
            this.location = route_star['StarSystem']
            this.system_pos = route_star['StarPos']
            this.current_star_class = route_star['StarClass']

        if route_star['StarSystem'] == this.location:
            reached_current_location_in_route = True
        if not reached_current_location_in_route:
            continue

        next_route_star = this.nav_route[i + 1]

        jump_distance = get_distance_between(route_star['StarPos'], next_route_star['StarPos'])
        # Calculate the effective distance for white dwarf and neutron jumps. Only applies if the jump distance is above maximum range.
        supercharge_ratio = 1
        if jump_distance > this.max_jump_range:
            if route_star['StarClass'].startswith('D'):
                # White dwarf, charging here results in a 50% range boost for the same fuel
                supercharge_ratio = 1.5
            if route_star['StarClass'].startswith('N'):
                # Neutron star, charging here results in a 400% range boost for the same fuel
                supercharge_ratio = 4
        effective_jump_distance = jump_distance / supercharge_ratio

        fuel_required_for_jump = fuel_for_jump(effective_jump_distance, cumulative_fuel_used)
        logger.debug(f"{fuel_required_for_jump:.4f} fuel estimated for jump from {route_star['StarSystem']} to {next_route_star['StarSystem']} (class {next_route_star['StarClass']})")

        cumulative_fuel_used += fuel_required_for_jump
        if not found_last_fuel_index and cumulative_fuel_used > this.main_tank:
            found_last_fuel_index = True
            this.last_route_index_we_have_fuel_for = i

        route_star['jump_distance'] = jump_distance
        route_star['jump_fuel'] = fuel_required_for_jump
        route_star['supercharge'] = supercharge_ratio
    logger.debug("--------  End jump fuel estimates  --------")


def journal_entry(cmdr, is_beta, system, station, entry, state):
    if entry['event'] == 'Loadout':
        this.loadout_max_jump_range = entry['MaxJumpRange'] # For debugging only at the moment, since we calculate this ourselves


    if entry['event'] == 'StartJump' and entry['JumpType'] == 'Hyperspace':
        if entry['StarSystem'] != this.location:
            logger.debug(f"Jump starting: {this.location} -> {entry['StarSystem']}")
            this.pause_fuel_calculations = True
            this.current_star_class = entry['StarClass']

            if this.nav_route is not None and this.location is not None:
                for i,star in enumerate(this.nav_route):
                    if star['StarSystem'] == this.location:
                        distance = get_distance_between(star['StarPos'], this.nav_route[i+1]['StarPos'])
                        fuel = fuel_for_jump(distance)
                        this.last_fuel_estimate = fuel
                        continue


    if 'ShipType' in state and state['ShipType'] is not None:
        this.ship_id = state['ShipType']

    if 'Cargo' in state:
        this.cargo_mass = 0
        for cargo_item in state['Cargo']:
            this.cargo_mass += state['Cargo'][cargo_item]

    if 'Modules' in state and state['Modules'] is not None:
        this.guardian_booster_data = None
        this.fsd_data = None
        this.module_mass = 0
        this.fuel_capacity = 0

        for slot in state['Modules']:
            module_id = state["Modules"][slot]["Item"]

            if module_id not in CORIOLIS_DATA["modules"]:
                if any(ignorable_module in module_id for ignorable_module in ["voicepack", "cockpit", "cargobaydoor", "approachsuite", "decal", "paintjob"]):
                    # Module is a paintjob, canopy, voicepack, etc that doesn't affect mass
                    continue
                logger.warn(f"Module {module_id} not found in coriolis data")
                continue

            accounted_for_mass = False

            # Check for modules with engineered mass modifications first, since those values
            # override the standard module mass.
            if "Engineering" in state["Modules"][slot]:
                # Engineering modifications provide the new mass, so no need to check the coriolis data if one is present
                for key,value in state["Modules"][slot]['Engineering'].items():
                    if key == "Modifiers":
                        for modifier in value:
                            if modifier['Label'] == "Mass":
                                this.module_mass += modifier['Value']
                                accounted_for_mass = True

            if not accounted_for_mass:
                # Include the standard module mass if the module isn't engineered in a way that
                # affects its mass.
                # Note: Armour/bulkhead mass is different per ship, but the ship type is accounted
                # for in the module id, and they are stored separately in the coriolis data.
                this.module_mass += CORIOLIS_DATA["modules"][module_id]["mass"]


            if "fueltank" in module_id:
                this.fuel_capacity += CORIOLIS_DATA["modules"][module_id]["capacity"]

            if "guardianfsdbooster" in module_id:
                # These only work when switched on, but the API doesn't appear to update this
                # property on state changes.
                #if state["Modules"][slot]["On"] == True:
                this.guardian_booster_data = CORIOLIS_DATA["modules"][module_id]

            if "hyperdrive" in module_id:
                this.fsd_data = CORIOLIS_DATA["modules"][module_id]

                if "Engineering" in state["Modules"][slot]:
                    # Override default values with engineered values if present
                    for key,value in state["Modules"][slot]['Engineering'].items():
                        if key == "Modifiers":
                            for modifier in value:
                                if modifier['Label'] == "FSDOptimalMass":
                                    this.fsd_data["optimal_mass"] = modifier['Value']
                                if modifier['Label'] == "MaxFuelPerJump":
                                    this.fsd_data["maximum_fuel"] = modifier['Value']



    if entry['event'] == 'NavRoute':
        # FIXME: This event is not generated if a route is re-plotted from the galaxy map (or probably other replottings). This can result in an incorrect route length in the 'remaining jumps' field and incorrect route info being displayed. Cancelling a route from the navmap also does not appear to generate a ClearNavRoute event.
        this.route_index = 0
        this.nav_route = state['NavRoute']['Route']
        process_nav_route()
        update_status()

    if entry['event'] == 'ClearNavRoute':
        this.nav_route = None
        update_status()

    if entry['event'] in ['Location', 'FSDJump']:
        this.location = entry['StarSystem']
        if 'StarPos' in entry:
            this.system_pos = entry['StarPos']
        else:
            this.system_pos = (None, None, None)

        if 'StarClass' in entry:
            this.current_star_class = entry['StarClass']

        if entry['event'] == "FSDJump":
            this.last_jump_dist = entry['JumpDist']
            this.last_fuel_used = entry['FuelUsed']
            this.pause_fuel_calculations = False
            if this.nav_route is not None:
                if get_route_index_of(this.location) == len(this.nav_route) -1:
                    # We've reached the end of the route
                    this.nav_route = None

                # Calculate how far along the route we can get with the current fuel supply
                if this.fsd_data is not None:
                    process_nav_route()
                    update_status()


def dashboard_entry(cmdr: str, is_beta: bool, entry: Dict[str, Any]) -> None:
    if "Fuel" in entry:
        main_fuel_level_changed = False
        if "FuelReservoir" in entry["Fuel"]:
            this.reservoir = entry["Fuel"]["FuelReservoir"]
        if "FuelMain" in entry["Fuel"]:
            main_fuel_level_changed = (entry['Fuel']['FuelMain'] != this.main_tank)
            this.main_tank = entry["Fuel"]["FuelMain"]
        # (Re-)Calculate how far along the route we can get with the current fuel supply
        if main_fuel_level_changed and this.nav_route is not None and this.fsd_data is not None:
            process_nav_route()
        update_status()

    if "Flags" in entry:
        if entry["Flags"] & edmc_data.FlagsInMainShip or entry["Flags"] & edmc_data.FlagsInSRV:
            this.show_fuel = True
        else:
            this.show_fuel = False

        if entry["Flags"] & edmc_data.FlagsInMainShip:
            this.show_jumps = True
        else:
            this.show_jumps = False


def update_status() -> None:
    if this.pause_fuel_calculations:
        return

    pip_width  = 14
    pip_height = 10
    pip_padding = 1
    pip_border_width = 1

    top_line_row = 0
    pip_row = 1
    bottom_line_row = 2

    #TODO: Make configurable
    max_pips_to_show = 12
    spacing_after_current_star = 3

    # Remove existing fuel jump indicators
    for widget in this.fuel_jumps_value_container.winfo_children():
        widget.destroy()


    # Calculate fuel level related stuff
    fuel_colour = COLOUR_NORMAL
    fuel_percentage = min(this.main_tank / this.fuel_capacity, 1) * 100
    if fuel_percentage < 25 or (this.fsd_data is not None and this.main_tank < this.fsd_data["maximum_fuel"]):
        fuel_colour = COLOUR_RED



    # Do the complicated bit

    total_jumps = 0
    jumps_completed = None
    current_star_primary_colour, current_star_secondary_colour = star_class_to_fuel_colour(this.current_star_class)

    current_star_line_colour = COLOUR_NORMAL if this.main_tank > 0 else COLOUR_DIMMED


    # Notches
    tk.Frame(
        master = this.fuel_jumps_value_container,
        background = current_star_line_colour,
        width = 1,
    ).grid(row=top_line_row, column=0, padx=0, pady=0, sticky=tk.N+tk.S)
    tk.Frame(
        master = this.fuel_jumps_value_container,
        background = current_star_line_colour,
        width = 1,
    ).grid(row=bottom_line_row, column=0, padx=0, pady=0, sticky=tk.N+tk.S)

    # Pip
    if this.current_star_class is None or this.current_star_class == "":
        # Empty pip
        tk.Frame(
            master = this.fuel_jumps_value_container,
            height = pip_height,
            width = pip_width,
        ).grid(row=pip_row, column=1, padx=pip_padding, pady=0, sticky=tk.W+tk.E)
    else:
        # Star class appropriate coloured pip
        tk.Frame(
            master = this.fuel_jumps_value_container,
            highlightthickness = pip_border_width,
            background = current_star_primary_colour,
            highlightbackground = current_star_secondary_colour,
            height = pip_height,
            width = pip_width,
        ).grid(row=pip_row, column=1, padx=pip_padding, pady=0, sticky=tk.W+tk.E)
    # Lines
    tk.Frame(
        master = this.fuel_jumps_value_container,
        background = current_star_line_colour,
        height = 1,
    ).grid(row=top_line_row, column=1, padx=0, pady=(0,pip_padding), sticky=tk.W+tk.E)
    tk.Frame(
        master = this.fuel_jumps_value_container,
        background = current_star_line_colour,
        height = 1,
    ).grid(row=bottom_line_row, column=1, padx=0, pady=(pip_padding,0), sticky=tk.W+tk.E)

    # Notches (end with spacing)
    tk.Frame(
        master = this.fuel_jumps_value_container,
        background = current_star_line_colour,
        width = 1,
    ).grid(row=top_line_row, column=2, padx=(0,spacing_after_current_star), pady=0, sticky=tk.N+tk.S)
    tk.Frame(
        master = this.fuel_jumps_value_container,
        background = current_star_line_colour,
        width = 1,
    ).grid(row=bottom_line_row, column=2, padx=(0,spacing_after_current_star), pady=0, sticky=tk.N+tk.S)

    # Notches (start)
    if this.nav_route is None or this.last_route_index_we_have_fuel_for > 0:
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = current_star_line_colour,
            width = 1,
        ).grid(row=top_line_row, column=3, padx=0, pady=0, sticky=tk.N+tk.S)
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = current_star_line_colour,
            width = 1,
        ).grid(row=bottom_line_row, column=3, padx=0, pady=0, sticky=tk.N+tk.S)


    if not this.nav_route:
        # Show fuel tank level
        #TODO: Set minimum width to prevent fuel bar being shown as 1px wide
        this.fuel_jumps_label.config(text = _("Fuel tank:"))
        this.fuel_jumps_value_container.grid(sticky=tk.W+tk.E, columnspan=1)

        fuel_percent = round(100 * this.main_tank / this.fuel_capacity)

        # Bar
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = fuel_colour,
            height = pip_height,
        ).grid(row=pip_row, column=5, padx=pip_padding, pady=0, sticky=tk.W+tk.E)
        # Lines
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = COLOUR_NORMAL,
            height = 1,
        ).grid(row=top_line_row, column=5, padx=0, pady=(0,pip_padding), sticky=tk.W+tk.E)
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = COLOUR_NORMAL,
            height = 1,
        ).grid(row=bottom_line_row, column=5, padx=0, pady=(pip_padding,0), sticky=tk.W+tk.E)

        # Lines
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = COLOUR_NORMAL,
            height = 1,
        ).grid(row=top_line_row, column=6, padx=0, pady=(0,pip_padding), sticky=tk.W+tk.E)
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = COLOUR_NORMAL,
            height = 1,
        ).grid(row=bottom_line_row, column=6, padx=0, pady=(pip_padding,0), sticky=tk.W+tk.E)

        # Notches
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = COLOUR_NORMAL,
            width = 1,
        ).grid(row=top_line_row, column=7, padx=0, pady=0, sticky=tk.N+tk.S)
        tk.Frame(
            master = this.fuel_jumps_value_container,
            background = COLOUR_NORMAL,
            width = 1,
        ).grid(row=bottom_line_row, column=7, padx=0, pady=0, sticky=tk.N+tk.S)

        # Make the fuel bar the right length
        this.fuel_jumps_value_container.grid_columnconfigure(5, weight=fuel_percent)
        this.fuel_jumps_value_container.grid_columnconfigure(6, weight=100 - fuel_percent)

        # Text
        this.fuel_level_label.config(
            foreground = fuel_colour,
            text = f'{l10n.Locale.string_from_number(this.main_tank, 1)} tonnes ({fuel_percentage:.0f}%)',
        )

    else:
        # Show nav route and scoopable stars
        #TODO: Limit maximum number of stars shown to avoid excessively wide EDMC windows
        this.fuel_jumps_label.config(text = _("Jumps in fuel range:"))
        this.fuel_jumps_value_container.grid(sticky=tk.W, columnspan=2)

        this.fuel_level_label.config(text = '')

        total_jumps = len(this.nav_route) - 1
        was_in_fuel_range = True
        in_fuel_range = True
        notch_offset = 0
        pip_column = 5   # Only defined here so it's available outside the loop
        pips_shown = 1
        for i, star in enumerate(this.nav_route):
            if jumps_completed is not None and pips_shown <= max_pips_to_show:
                pips_shown += 1
                pip_column = 5 + i - jumps_completed + notch_offset
                in_fuel_range = (i<=this.last_route_index_we_have_fuel_for)

                # End of fuel range notches (Don't show immediately after the current star as that already has 'end' notches)
                if was_in_fuel_range and not in_fuel_range and pips_shown > 2:
                    # Notches
                    tk.Frame(
                        master = this.fuel_jumps_value_container,
                        background = current_star_primary_colour,
                        width = 1,
                    ).grid(row=top_line_row, column=pip_column, padx=0, pady=0, sticky=tk.N+tk.S)
                    tk.Frame(
                        master = this.fuel_jumps_value_container,
                        background = current_star_primary_colour,
                        width = 1,
                    ).grid(row=bottom_line_row, column=pip_column, padx=0, pady=0, sticky=tk.N+tk.S)
                    notch_offset += 1
                    pip_column += 1
                was_in_fuel_range = in_fuel_range

                # End of route notches (not shown if not all route pips are displayed)
                if i == len(this.nav_route)-1 and in_fuel_range:
                    # Notches
                    tk.Frame(
                        master = this.fuel_jumps_value_container,
                        background = current_star_primary_colour,
                        width = 1,
                    ).grid(row=top_line_row, column=pip_column+1, padx=0, pady=0, sticky=tk.N+tk.S)
                    tk.Frame(
                        master = this.fuel_jumps_value_container,
                        background = current_star_primary_colour,
                        width = 1,
                    ).grid(row=bottom_line_row, column=pip_column+1, padx=0, pady=0, sticky=tk.N+tk.S)

                if in_fuel_range:
                    # Lines
                    tk.Frame(
                        master = this.fuel_jumps_value_container,
                        background = COLOUR_NORMAL,
                        height = 1,
                    ).grid(row=top_line_row, column=pip_column, padx=0, pady=(0,pip_padding), sticky=tk.W+tk.E)
                    tk.Frame(
                        master = this.fuel_jumps_value_container,
                        background = COLOUR_NORMAL,
                        height = 1,
                    ).grid(row=bottom_line_row, column=pip_column, padx=0, pady=(pip_padding,0), sticky=tk.W+tk.E)

                # Star class appropriate coloured pip
                primary_colour, secondary_colour = star_class_to_fuel_colour(star['StarClass'])
                tk.Frame(
                    master = this.fuel_jumps_value_container,
                    highlightthickness = pip_border_width,
                    background = primary_colour if in_fuel_range else COLOUR_GREYED,
                    highlightbackground = secondary_colour if in_fuel_range else COLOUR_GREYED,
                    height = pip_height,
                    width = pip_width,
                ).grid(row=pip_row, column=pip_column, padx=pip_padding, pady=0, sticky=tk.W+tk.E)

            if star['StarSystem'] == this.location:
                jumps_completed = i


    # Route progress summary
    if this.nav_route is None or this.show_jumps is False:
        this.route_progress_value["text"] = f'{_("---")} / {_("---")}'
        this.route_progress_value["foreground"] = COLOUR_DIMMED
    else:
        if jumps_completed is None:
            jumps_completed = 0
        remaining = l10n.Locale.string_from_number(total_jumps - jumps_completed, 0)
        total = l10n.Locale.string_from_number(total_jumps, 0)
        total_distance = 0
        add_distance = False
        for star in this.nav_route:
            if star['StarSystem'] == this.location:
                add_distance = True
            if add_distance and 'jump_distance' in star:
                # This won't be present for the last star in the route, since there's no jump from there
                total_distance += star['jump_distance']
        this.route_progress_value["text"] = f'{remaining} / {total} ({total_distance:.1f} ly)'
        this.route_progress_value["foreground"] = COLOUR_NORMAL


    ### DEBUG ###

    # tk.Label(
    #     master = this.fuel_jumps_value_container,
    #     text = "max fuel / range / loadout range / boost",
    # ).grid(row=5, column=0, sticky=tk.E)
    # tk.Label(
    #     master = this.fuel_jumps_value_container,
    #     text = f"{l10n.Locale.string_from_number(this.fsd_data['maximum_fuel'], 2)} / {l10n.Locale.string_from_number(maximum_range, 2)} / {l10n.Locale.string_from_number(this.loadout_max_jump_range, 2)} / {l10n.Locale.string_from_number(range_boost, 2)}",
    # ).grid(row=5, column=1, sticky=tk.E)

    # error = this.last_fuel_used - this.last_fuel_estimate
    # error_percent = error * 100 / this.last_fuel_used if this.last_fuel_used > 0 else 0

    # tk.Label(
    #     master = this.fuel_jumps_value_container,
    #     text = "error",
    # ).grid(row=16, column=12, sticky=tk.E)
    # tk.Label(
    #     master = this.fuel_jumps_value_container,
    #     text = f"{l10n.Locale.string_from_number(error, 4)} ({error_percent:.1f}%)",
    # ).grid(row=16, column=13, sticky=tk.E)

    ### DEBUG ###

    # Make sure the fuel jump widgets are themed correctly
    theme.update(this.fuel_jumps_value_container)
